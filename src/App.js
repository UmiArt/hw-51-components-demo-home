import React from "react";
import Numbers,{randomN} from "./Number/Numbers";

class App extends React.Component {

    state = {
        number: [
            {num: (randomN(5, 36))},
            {num: (randomN(5, 36))},
            {num: (randomN(5, 36))},
            {num: (randomN(5, 36))},
            {num: (randomN(5, 36))}
        ].sort((a, b) => (a.num > b.num) ? 1 : -1)
    };

    clickBtn = () => {
        let arr = [];
        while (arr.length < 5) {
            let newNumber = randomN(5, 36)
            if (arr.indexOf(newNumber) === -1) arr.push(newNumber);
        }

        this.setState({
            number: [
                {num: arr[0]},
                {num: arr[1]},
                {num: arr[2]},
                {num: arr[3]},
                {num: arr[4]}
            ].sort((a, b) => (a.num > b.num) ? 1 : -1)
        });
    };

    render() {
        return (
            <div className="app">
                <div className="top"></div>
                <button className="btn" onClick={this.clickBtn}>New numbers</button>
                <Numbers numNum={this.state.number[0].num}
                         numNum2={this.state.number[1].num}
                         numNum3={this.state.number[2].num}
                         numNum4={this.state.number[3].num}
                         numNum5={this.state.number[4].num}/>
            </div>
        );
    };
}

export default App;
